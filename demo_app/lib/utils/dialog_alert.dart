import 'package:flutter/material.dart';

class PopupDialog {
  Future<void> ackAlert(BuildContext bContext, String title, String body) {
    return showDialog<void>(
      context: bContext,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(title),
          content: Text(body),
          actions: <Widget>[
            FlatButton(
              child: Text('Ok'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}