import 'dart:async';
import 'package:shared_preferences/shared_preferences.dart';

class SharePreferences {
  Future getSharedPreferences() {
    return SharedPreferences.getInstance();
  }

  Future getString(String name) async {
    SharedPreferences prefs = await getSharedPreferences();

    return prefs.getString(name);
  }

  Future getBool(String name) async {
    SharedPreferences prefs = await getSharedPreferences();

    return prefs.getBool(name);
  }

  Future getDouble(String name) async {
    SharedPreferences prefs = await getSharedPreferences();

    return prefs.getDouble(name);
  }

  Future getInt(String name) async {
    SharedPreferences prefs = await getSharedPreferences();

    return prefs.getInt(name);
  }

  Future getList(String name) async {
    SharedPreferences prefs = await getSharedPreferences();

    return prefs.getStringList(name);
  }

}