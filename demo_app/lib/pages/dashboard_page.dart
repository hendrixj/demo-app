import 'package:demo_app/pages/widget/list_movie.dart';
import 'package:demo_app/resources/movie_api_provider.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:demo_app/common/global.dart' as global;

class DashboardPage extends StatefulWidget {
  @override
  _DashboardPageState createState() => _DashboardPageState();
}

class _DashboardPageState extends State<DashboardPage> {

  DateFormat formatter = new DateFormat('dd MMM yyyy');

  @override
  Widget build(BuildContext context) {
    return global.kShowRecommend ? ListView(
      children: <Widget>[
        FutureBuilder(
            future: MovieApiProvider().fetchRecommendedMovie(),
            builder: (context, AsyncSnapshot snapshot) {
              if (snapshot.hasData) {
                return detailMovie(snapshot.data);
              } else if (snapshot.hasError) {
                return Text(snapshot.error.toString());
              }
              return Center(child: CircularProgressIndicator());
            },
          ),
      ],
    ) : Container();
  }

  Widget detailMovie(Map<String, dynamic> movie) {
    return Container(
      padding: EdgeInsets.all(24),
      child: Column(
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(bottom: 15),
            height: 400,
            width: 400,
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: NetworkImage(movie['poster']), fit: BoxFit.cover)),
          ),
          _textReadOnly("Title", movie['title']),
          _textReadOnly("Year", movie['year']),
          _textReadOnly("Label", movie['label']),
          _textReadOnly("Priority", movie['priority']),
          _textReadOnly("Rating", movie['rating']),
          _textReadOnly("Date Time", movie['timestamp'])
        ],
      ),
    );
  }

  TextStyle _textStyle = TextStyle(fontSize: 20);

  Widget _textReadOnly(String key, dynamic value) {
    return Container(
      margin: EdgeInsets.only(bottom: 15),
      child: Row(
        children: <Widget>[
          Text(
            "$key :",
            style: _textStyle,
            maxLines: 2,
            softWrap: true,
          ),
          Expanded(
            child: Container(
              margin: EdgeInsets.only(left: 20),
              child: Text(
                key == "Date Time" ? formatter.format(DateTime.fromMicrosecondsSinceEpoch(value * 1000)) : value.toString(),
                maxLines: 1,
                style: _textStyle,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
