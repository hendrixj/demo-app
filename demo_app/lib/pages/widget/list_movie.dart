import 'package:demo_app/models/movie_item.dart';
import 'package:flutter/material.dart';

class ListGridMovie extends StatelessWidget {
  final List<dynamic> movie;
  final Function(Map movie) deleteMovie;

  ListGridMovie(this.movie, this.deleteMovie);

  @override
  Widget build(BuildContext context) {
    return gridBuildList(movie);
  }

  Widget gridBuildList(List<dynamic> data) {
    return GridView.builder(
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
          childAspectRatio: 0.6,
        ),
        itemCount: data.length,
        itemBuilder: (context, int index) {
          Map<String, dynamic> _movieItem = data[index];
//          return Container();
          return Card(
              margin: EdgeInsets.all(5),
              shape: RoundedRectangleBorder(),
              elevation: 5,
              child: Column(
                children: <Widget>[
                  _movieItem['poster'] == "N/A"
                      ? noHaveImage()
                      : haveImage(_movieItem['poster']),
                  _movieItemBottomText(_movieItem, index),
                ],
              ));
        });
  }

  Icon noHaveImage() {
    return Icon(Icons.image, size: 200);
  }

  Container haveImage(String poster) {
    return Container(
      height: 200,
      width: 200,
      decoration: BoxDecoration(
          image:
              DecorationImage(image: NetworkImage(poster), fit: BoxFit.cover)),
    );
  }

  Container _movieItemBottomText(Map<String, dynamic> _movieItem, int index) {
    return Container(
        padding: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
        height: 130,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    _movieItem['title'],
                    style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                    softWrap: true,
                    maxLines: 2,
                  ),
                  Text(
                    _movieItem['label'],
                    style: TextStyle(fontSize: 16),
                  ),
                  Text(
                    "Rating: ${_movieItem['rating']}",
                    style: TextStyle(fontSize: 16),
                  ),
                  Text(
                    "Priorit:${_movieItem['priority']}",
                    style: TextStyle(fontSize: 16),
                  ),
                  Text(
                    _movieItem['year'],
                    style: TextStyle(fontSize: 16),
                  )
                ],
              ),
            ),
            FloatingActionButton(
              onPressed: () {
                deleteMovie(_movieItem);
              },
              heroTag: index,
              child: Icon(Icons.remove),
              mini: true,
            )
          ],
        ));
  }
}
