import 'package:demo_app/pages/login_page.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:demo_app/common/global.dart' as global;

class SettingPage extends StatefulWidget {
  @override
  _SettingPageState createState() => _SettingPageState();
}

class _SettingPageState extends State<SettingPage> {
  Future<void> _buttonLogout() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.remove("token");
    global.kToken = null;
    global.kShowRecommend = false;
    Navigator.of(context).pushReplacement(
        MaterialPageRoute(builder: (context) => LoginScreenPage()));
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(24),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          CheckboxListTile(
            value: global.kShowRecommend,

            onChanged: (value) {
              setState(() {
                global.kShowRecommend = value;
              });
            },
            title: Text("Show Recommended", style: TextStyle(fontSize: 20)  ),
          ),
          GestureDetector(
            onTap: () {
              _buttonLogout();
            },
            child: Container(
              width: MediaQuery
                  .of(context)
                  .size
                  .width,
              padding: EdgeInsets.all(10),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(15),
                color: Colors.lightBlueAccent,
              ),
              child: Text(
                "Logout",
                style: TextStyle(fontSize: 20.0),
                textAlign: TextAlign.center,
              ),
            ),
          )
        ],
      ),
    );
  }
}
