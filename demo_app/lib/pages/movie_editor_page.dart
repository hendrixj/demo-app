import 'package:demo_app/models/movie_item.dart';
import 'package:demo_app/resources/movie_api_provider.dart';
import 'package:demo_app/utils/dialog_alert.dart';
import 'package:flutter/material.dart';

class MovieEditorPage extends StatefulWidget {
  MovieEditorPage(this.movieItem);

  MovieSearch movieItem;

  @override
  _MovieEditorPageState createState() => _MovieEditorPageState();
}

class _MovieEditorPageState extends State<MovieEditorPage> {
  TextEditingController _labelController = new TextEditingController();
  TextEditingController _priorityController = new TextEditingController();
  TextEditingController _ratingController = new TextEditingController();
  bool _viewed = true;
  final _formKey = GlobalKey<FormState>();

  @override
  void dispose(){
    super.dispose();
  }

  TextStyle _textStyle = TextStyle(fontSize: 20);

  void _saveMovie() async {
    _formKey.currentState.save();
    if (_formKey.currentState.validate()) {
      Map<String, dynamic> requestBody = {
        "id": widget.movieItem.id,
        "title": widget.movieItem.title,
        "year": widget.movieItem.year,
        "poster": widget.movieItem.poster,
        "label": _labelController.text,
        "priority": int.parse(_priorityController.text),
        "viewed": _viewed,
        "rating": int.parse(_ratingController.text),
        "timestamp": DateTime.now().millisecondsSinceEpoch,
      };

      String response =
          await MovieApiProvider().postMovie(requestBody);
      await PopupDialog().ackAlert(context, "Response", response);
      Navigator.pop(context);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Movie Editor"),
        actions: <Widget>[
          IconButton(
            onPressed: () {
              _saveMovie();
            },
            icon: Icon(Icons.check),
            iconSize: 26,
          )
        ],
      ),
      body: ListView(children: <Widget>[
        Form(
          key: _formKey,
          child: Container(
            padding: EdgeInsets.all(24),
            child: Column(
              children: <Widget>[
                _poster(),
                _textReadOnly("Title", widget.movieItem.title),
                _textReadOnly("Year", widget.movieItem.year),
                _textField("Label"),
                _textField("Priority"),
                _textField("Rating"),
                _switchButton(),
              ],
            ),
          ),
        ),
      ]),
    );
  }

  Widget _textReadOnly(String key, String value) {
    return Container(
      margin: EdgeInsets.only(bottom: 15),
      child: Row(
        children: <Widget>[
          Text(
            "$key :",
            style: _textStyle,
          ),
          Container(
            margin: EdgeInsets.only(left: 20),
            child: Text(
              value,
              softWrap: false,
              maxLines: 1,
              style: _textStyle,
            ),
          )
        ],
      ),
    );
  }

  Widget _textField(String label) {
    return Container(
      margin: EdgeInsets.only(bottom: 15),
      child: TextFormField(
        decoration: InputDecoration(
            border: OutlineInputBorder(),
            labelText: label,
            labelStyle: _textStyle),
        controller: label == "Label"
            ? _labelController
            : (label == "Priority" ? _priorityController : _ratingController),
        validator: (value) {
          if (value.isEmpty) {
            return 'Please enter ${label}';
          }
          return null;
        },
      ),
    );
  }

  Widget _switchButton() {
    return Container(
        margin: EdgeInsets.only(bottom: 15),
        child: SwitchListTile(
            contentPadding: EdgeInsets.all(0),
            title: Text(
              'Viewed',
              style: _textStyle,
            ),
            value: _viewed,
            onChanged: (bool val) => setState(() => _viewed != _viewed)));
  }

  Widget _poster() {
    return Container(
      height: 300,
      width: 300,
      margin: EdgeInsets.only(bottom: 15),
      decoration: BoxDecoration(
          image: DecorationImage(
              image: NetworkImage(widget.movieItem.poster), fit: BoxFit.cover)),
    );
  }
}
