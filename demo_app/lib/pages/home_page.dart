import 'package:demo_app/pages/dashboard_page.dart';
import 'package:demo_app/pages/favorite_page.dart';
import 'package:demo_app/pages/place_holder.dart';
import 'package:demo_app/pages/search_page.dart';
import 'package:demo_app/pages/setting_page.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int _currentIndex = 0;
  String _titleAppBar = "Dashboard";
  final List<Widget> _children = [
    DashboardPage(),
    SearchPage(),
    FavoritePage(),
    SettingPage()
  ];

  List<String> _listTitleMenu = [
    "Dashboard",
    "Search Movie",
    "Favorite Movie",
    "Settings"
  ];

  void onTabTapped(int index) {
    setState(() {
      _currentIndex = index;
      _titleAppBar = _listTitleMenu[index];
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      child: Scaffold(
        appBar: AppBar(
          title: Text(_titleAppBar),
        ),

        body: _children[_currentIndex],
        bottomNavigationBar: bottomNavigationBarList()),
      onWillPop: null);
  }

  BottomNavigationBar bottomNavigationBarList() {
    return BottomNavigationBar(
        currentIndex: _currentIndex,
        onTap: onTabTapped,
        items: [
          bottomNavigationBarItem("Dashboard"),
          bottomNavigationBarItem("Search"),
          bottomNavigationBarItem("Favorite"),
          bottomNavigationBarItem("Settings"),
        ]);
  }

  BottomNavigationBarItem bottomNavigationBarItem(String item) {
    IconData _icon;
    if (item == "Dashboard") {
      _icon = Icons.dashboard;
    } else if (item == "Search") {
      _icon = Icons.search;
    } else if (item == "Favorite") {
      _icon = Icons.favorite;
    } else {
      _icon = Icons.settings;
    }

    return BottomNavigationBarItem(
      icon: new Icon(
        _icon,
        color: Color(0xFF828d99),
      ),
      title: new Text(
        item,
        style: TextStyle(
            color: Color(0xFF828d99), fontFamily: "Sailec", fontSize: 12.0),
      ),
    );
  }

  Widget drawerListItem(String name, int index) {
    return Container(
      decoration: BoxDecoration(
        border: Border(bottom: BorderSide(width: 1)),
        color:
            _currentIndex == index ? Colors.red : Colors.lightBlueAccent[100],
      ),
      child: ListTile(
        onTap: () {
          onTabTapped(index);
        },
        title: Text(name),
      ),
    );
  }
}
