import 'package:demo_app/bloc/movies_bloc.dart';
import 'package:demo_app/models/movie_item.dart';
import 'package:demo_app/pages/movie_editor_page.dart';
import 'package:flutter/material.dart';

class SearchPage extends StatefulWidget {
  @override
  _SearchPageState createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {
  TextEditingController _searchController = new TextEditingController();
  String _year;
  int _page = 1;
  List<String> _listYear =
      new List<String>.generate(30, (i) => (1990 + i).toString());

  Stream asd;

  @override
  void initState() {

    _listYear.add("");
    bloc.fetchAllMovies(_year, _page);
  }

  @override
  void dispose() {
    _searchController.dispose();
    super.dispose();
  }

  _searchButton() {
    setState(() {
      _page = 1;
    });
    bloc.fetchAllMovies(_year, _page, title: _searchController.text);
  }

  void addMovie(MovieSearch _movie) {

    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => MovieEditorPage(_movie)),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.max,
      children: <Widget>[
        headerMenuSearch(),
        Divider(
          height: 10,
          color: Colors.black,
        ),
        Expanded(
          child: StreamBuilder(
            stream: bloc.allMovies,
            builder: (context, AsyncSnapshot<MovieSearchModel> snapshot) {
              if (snapshot.hasData) {
                return gridBuildList(snapshot);
              } else if (snapshot.hasError) {
                return Text(snapshot.error.toString());
              }
              return Center(child: CircularProgressIndicator());
            },
          ),
        ),
      ],
    );
  }

  Widget headerMenuSearch() {
    return Container(
        padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Expanded(
                child: Container(
              child: TextField(
                decoration: InputDecoration(border: OutlineInputBorder()),
                controller: _searchController,
              ),
            )),
            Container(
              width: 100,
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: DropdownButton<String>(
                itemHeight: 80,
                isExpanded: true,
                items: _listYear.map<DropdownMenuItem<String>>((String value) {
                  return DropdownMenuItem<String>(
                    value: value,
                    child: Text(value),
                  );
                }).toList(),
                value: _year,
                onChanged: (value) {
                  setState(() {
                    _year = value;
                  });
                },
              ),
            ),
            GestureDetector(
              onTap: () {
                _searchButton();
              },
              child: Padding(
                padding: EdgeInsets.only(left: 10),
                child: Icon(
                  Icons.search,
                  size: 30,
                ),
              ),
            )
          ],
        ));
  }

  Widget gridBuildList(AsyncSnapshot<MovieSearchModel> snapshot) {
    return GridView.builder(
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
          childAspectRatio: 0.7,
        ),
        itemCount: snapshot.data.results.length,
        itemBuilder: (context, int index) {
          MovieSearch _movieItem = snapshot.data.results[index];
          return Card(
              margin: EdgeInsets.all(5),
              shape: RoundedRectangleBorder(),
              elevation: 5,
              child: Column(
                children: <Widget>[
                  _movieItem.poster == "N/A"
                      ? noHaveImage()
                      : haveImage(_movieItem.poster),
                  _movieItemBottomText(_movieItem, index),
                ],
              ));
        });
  }

  Icon noHaveImage() {
    return Icon(Icons.image, size: 200);
  }

  Container haveImage(String poster) {
    return Container(
      height: 200,
      width: 200,
      decoration: BoxDecoration(
          image:
              DecorationImage(image: NetworkImage(poster), fit: BoxFit.cover)),
    );
  }

  Container _movieItemBottomText(MovieSearch _movie, int index) {
    return Container(
        padding: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
        height: 80,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    _movie.title,
                    style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                    softWrap: true,
                    maxLines: 2,
                  ),
                  Text(
                    _movie.year,
                    style: TextStyle(fontSize: 16),
                  ),
                ],
              ),
            ),
            FloatingActionButton(
              onPressed: () {
                addMovie(_movie);
              },
              heroTag: index,
              child: Icon(Icons.add),
              mini: true,
            )
          ],
        ));
  }
}
