import 'package:demo_app/models/movie_item.dart';
import 'package:demo_app/pages/widget/list_movie.dart';
import 'package:demo_app/resources/movie_api_provider.dart';
import 'package:flutter/material.dart';

class FavoritePage extends StatefulWidget {
  @override
  _FavoritePageState createState() => _FavoritePageState();
}

class _FavoritePageState extends State<FavoritePage> {

  @override
  void initState(){

  }

  Future<void> deleteMovie(_movie) async {
    await MovieApiProvider().deleteFavoriteMovie(_movie['id']);
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.max,
      children: <Widget>[
        Expanded(
          child: FutureBuilder<List<dynamic>>(
            future: MovieApiProvider().fetchFavoriteMovieList(),
            builder: (context, AsyncSnapshot snapshot) {
              if (snapshot.hasData) {
                return ListGridMovie(snapshot.data, deleteMovie);
              } else if (snapshot.hasError) {
                return Text(snapshot.error.toString());
              }
              return Center(child: CircularProgressIndicator());
            },
          ),
        ),
      ],
    );
  }
}
