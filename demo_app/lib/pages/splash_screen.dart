import 'dart:async';

import 'package:demo_app/pages/home_page.dart';
import 'package:demo_app/pages/login_page.dart';
import 'package:demo_app/utils/share_preferences.dart';
import 'package:flutter/material.dart';

class SplashScreenPage extends StatefulWidget {
  @override
  _SplashScreenPageState createState() => _SplashScreenPageState();
}

class _SplashScreenPageState extends State<SplashScreenPage> {

  String _tokenLogin;

  @override
  void initState() {
    super.initState();
    getTokenLogin();
    loadData();
  }

  Future<void> getTokenLogin() async {
    _tokenLogin = await SharePreferences().getString("token");
  }

  Future<Timer> loadData() async {
    return new Timer(Duration(seconds: 2), onDoneLoading);
  }

  onDoneLoading() async {
    Navigator.pushReplacementNamed(context, _tokenLogin != null ? "/home" : "/login");
//    Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => _tokenLogin != null ? HomePage() : LoginScreenPage()));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              height: 300,
              width: 300,
              decoration: BoxDecoration(
                  image: DecorationImage(image: AssetImage("images/ic_launcher.png"))
              ),
            ),
            Text(
              "Video Apps",
              style: TextStyle(
                fontSize: 30.0,
              ),
            )
          ],
        )
      ),
    );
  }
}