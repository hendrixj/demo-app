import 'package:demo_app/models/movie_item.dart';
import 'package:demo_app/resources/repository.dart';
import 'package:rxdart/rxdart.dart';

class MoviesBloc {
  final _repository = Repository();
  final _moviesFetcher = PublishSubject<MovieSearchModel>();

  Stream<MovieSearchModel> get allMovies => _moviesFetcher.stream;

  fetchAllMovies(String year, int page, {String title}) async {
    if(title == ""){
      title = null;
    }
    MovieSearchModel itemModel = await _repository.fetchAllMovies(title, year, page);
    _moviesFetcher.sink.add(itemModel);
  }

  dispose() {
    _moviesFetcher.close();
  }
}

final bloc = MoviesBloc();