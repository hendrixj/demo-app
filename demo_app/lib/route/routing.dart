import 'package:demo_app/pages/home_page.dart';
import 'package:demo_app/pages/login_page.dart';
import 'package:flutter/material.dart';

class RouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    final args = settings.arguments;

    switch (settings.name) {
      case Routes.initialPage:
        return MaterialPageRoute(builder: (_) => HomePage());
      case Routes.lognPage:
        return MaterialPageRoute(builder: (_) => LoginScreenPage());
      default:
        return _errorRoute();
    }
  }

  static Route<dynamic> _errorRoute() {
    return MaterialPageRoute(builder: (_) {
      return Scaffold(
        appBar: AppBar(
          title: Text('Error'),
        ),
        body: Center(
          child: Text('ERROR'),
        ),
      );
    });
  }
}

class Routes {
  static const String initialPage = '/home';
  static const String lognPage = '/login';
  static const String movieEditorPage = '/movie_editor';
}
