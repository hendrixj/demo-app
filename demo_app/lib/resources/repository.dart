import 'dart:async';
import 'package:demo_app/models/movie_item.dart';

import 'movie_api_provider.dart';

class Repository {
  final moviesApiProvider = MovieApiProvider();

  Future<MovieSearchModel> fetchAllMovies(String title, String year, int page) =>
      moviesApiProvider.fetchMovieSearchList(title, year, page);
}
