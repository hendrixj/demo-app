import 'dart:async';
import 'dart:io';
import 'package:demo_app/common/global.dart';
import 'package:demo_app/models/movie_item.dart';
import 'package:http/http.dart';
import 'dart:convert';

class MovieApiProvider {
  HttpClient httpClient = HttpClient();
  Client client = Client();
  final _apiKey = 'eb26c564';
  final _url = "https://demo-video-ws-chfmsoli4q-ew.a.run.app/video-ws/";
  final String _video = "videos/";
  final String _recommended = "recommended";

  bool _certificateCheck(X509Certificate cert, String host, int port) =>
      host == 'https://demo-video-ws-chfmsoli4q-ew.a.run.app/video-ws/videos';

  Map<String, String> headers = {
    AppHeaders.TOKEN_KEY: "",
    AppHeaders.CONTENT_TYPE_KEY: AppHeaders.JSON_HEADER,
    "accept": "application/json"
  };

  Future<MovieSearchModel> fetchMovieSearchList(
      String title, String year, int page) async {
    final response = await client.get(
        "http://www.omdbapi.com/?apikey=$_apiKey&y=$year&s=$title&page=$page");
    print(response.body.toString());
    if (response.statusCode == 200) {
      return MovieSearchModel.fromJson(json.decode(response.body));
    } else {
      throw Exception('Failed to load post');
    }
  }

  Future postMovie(Map body) async {
    HttpClientResponse response = await provider(_url+_video, "post", body: body);
    if (response.statusCode == 200) {
      return "Success";
    } else {
      return "Failed";
    }
  }

  Future<Map> fetchRecommendedMovie() async {
    HttpClientResponse response = await provider(_url+_recommended, "get");
    if (response.statusCode == 200) {
      return jsonDecode(await response.transform(utf8.decoder).join());
    } else {
      throw Exception('Failed to load post');
    }
  }

  Future<List<dynamic>> fetchFavoriteMovieList() async {
    HttpClientResponse response = await provider(_url+_video, "get");
    if (response.statusCode == 200) {
      return jsonDecode(await response.transform(utf8.decoder).join());
    } else {
      throw Exception('Failed to load post');
    }
  }

  Future deleteFavoriteMovie(String id) async {
    final String url = _url+_video+id;
    HttpClientResponse response = await provider(url, "delete");
    if (response.statusCode == 200) {
      return await response.transform(utf8.decoder).join();
    } else {
      throw Exception('Failed to load post');
    }
  }

  Future<HttpClientResponse> provider(String url, String type,
      {Map body}) async {
    httpClient.badCertificateCallback =
        ((X509Certificate cert, String host, int port) {
      final isValidHost = host == url;
      return isValidHost;
    });
    HttpClientRequest request;
    if (type == "get") {
      request = await httpClient.getUrl(Uri.parse(url));
    } else if (type == "post") {
      request = await httpClient.postUrl(Uri.parse(url));
    } else if (type == "delete") {
      request = await httpClient.deleteUrl(Uri.parse(url));
    }

    request.headers
        .set(AppHeaders.CONTENT_TYPE_KEY, 'application/x-www-form-urlencoded');
    request.headers.set(AppHeaders.TOKEN_KEY, kToken);

    if (body != null) {
      request.add(utf8.encode(json.encode(body)));
    }

    HttpClientResponse response = await request.close();

    return response;
  }
}
