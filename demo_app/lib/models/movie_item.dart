class MovieSearchModel {
  String _total_results;
  List<MovieSearch> _results = [];

  MovieSearchModel.fromJson(Map<String, dynamic> parsedJson) {
    _total_results = parsedJson['totalResults'];
    List<MovieSearch> temp = [];
    if (parsedJson['Response'] == "True") {
      for (int i = 0; i < parsedJson['Search'].length; i++) {
        MovieSearch result = MovieSearch(parsedJson['Search'][i]);
        temp.add(result);
      }
    }
    _results = temp;
  }

  List<MovieSearch> get results => _results;

  String get total_results => _total_results;
}

class MovieSearch {
  String _title;
  String _year;
  String _id;
  String _type;
  String _poster;

  MovieSearch(movie) {
    _title = movie['Title'];
    _year = movie['Year'];
    _id = movie["imdbID"];
    _type = movie["Type"];
    _poster = movie["Poster"];
  }

  String get title => _title;

  String get year => _year;

  String get id => _id;

  String get type => _type;

  String get poster => _poster;
}
